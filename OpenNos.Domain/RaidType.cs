﻿namespace OpenNos.Domain
{
    public enum RaidType : byte
    {
        //0 > 34
        MotherCuby = 0,
        Ginseng = 1,
        Castra = 2,
        GiantBlackSpider = 3,
        MassiveSlade = 4,
        ChickenKing = 5,
        Namaju = 6,
        GiantGrasslin = 7,
        HugeSnowman = 8,
        DesertRobberBand = 9,
        JackOLantern = 10,
        ChickenQueen = 11,
        CaptainPeteOPeng = 12,
        KertosTheDemonDog = 13,
        ValakusKingOfFire = 14,
        FireGodGrenigas = 15,
        LordDraco = 16,
        GlacerusTheIceCold = 17,
        NineTailedFoxy = 18,
        MaruTheMillennialTiger = 19,
        WitchLaurena = 20,
        HongbiAndCheongbi = 21,
        LolaLopears = 22,
        GoddessZenas = 23,
        GoddessErenia = 24,
        IncompleteFernon = 25,
        GreedyFafnir = 26,
        TwistedYertirand = 27,
        ProfessorMacavity = 28,
        MadMarchHare = 29,
        TwistedSpiritKingKirollas = 30,
        TwistedBeastKingCarno = 31,
        DemonGodBelial = 32,
        EvilOverlordPaimon = 33,
        RevenantPaimon = 34,
        LordHatus = 50,
        LordMorcos = 51,
        BaronBerios = 52,
        LadyCalvinas = 53,
        CaligorTreasureChest = 54,
        CaligorRegularTreasureChest = 55,
        CaligorGlowingTreasureChest = 56,
        LaurenaAncientWitchChest = 57,
        LaurenaMysteriousWitchChest = 58,
        LaurenaFabledWitchChest = 59,
        AncelloanTheCreatorTreasureChest = 60,
        FernonTheDestroyerTreasureChest = 61,
        QueenBee = 62,
    }
}
﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class donator : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "IsDonator", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Character", "IsDonator");
        }
    }
}

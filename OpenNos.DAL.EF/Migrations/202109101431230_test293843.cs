﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test293843 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CharacterSkill", "IsPartnerSkill", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CharacterSkill", "IsPartnerSkill");
        }
    }
}

﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class trophyupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "Trophy11", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "Trophy12", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "Trophy13", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "Trophy14", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "Trophy15", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Character", "Trophy15");
            DropColumn("dbo.Character", "Trophy14");
            DropColumn("dbo.Character", "Trophy13");
            DropColumn("dbo.Character", "Trophy12");
            DropColumn("dbo.Character", "Trophy11");
        }
    }
}

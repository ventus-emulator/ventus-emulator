INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
)
VALUES

(1, 1, 19, 0, 0, 0, 0, 30000, 50), /* 10x Angel's Feather */
(0, 20, 2282, 0, 0, 0, 0, 30000, 50), /* 20x Angel's Feather */
(0, 50, 2282, 0, 0, 0, 0, 30000, 30), /* 50x Angel's Feather */
(0, 100, 2282, 0, 0, 0, 0, 30000, 20), /* 100x Angel's Feather */
(0, 200, 2282, 0, 0, 0, 0, 30000, 10), /* 200x Angel's Feather */
(0, 999, 2282, 0, 0, 0, 0, 30000, 5); /* 999x Angel's Feather */


INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
)
VALUES

(0, 1, 2518, 0, 0, 0, 0, 9197, 50), /* Ruby of Completion */
(0, 1, 2519, 0, 0, 0, 0, 9197, 50), /* Sapphire of Completion */
(0, 1, 2520, 0, 0, 0, 0, 9197, 50), /* Obsidian of Completion */
(0, 1, 2521, 0, 0, 0, 0, 9197, 50); /* Topaz of Completion */


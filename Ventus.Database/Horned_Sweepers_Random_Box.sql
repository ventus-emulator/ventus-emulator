INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
)
VALUES

(0, 1, 5152, 0, 0, 0, 0, 5154, 10), /* Horned Sweeper */
(0, 1, 1119, 0, 0, 0, 0, 5154, 25), /* Medium Special Potion */
(0, 40, 2160, 0, 0, 0, 0, 5154, 25), /* Wings of Friendship */
(0, 1, 1279, 0, 0, 0, 0, 5154, 25), /* Pet Basket (30 Days) */
(0, 16, 1285, 0, 0, 0, 0, 5154, 25), /* Guardian Angel's Blessing */
(0, 14, 1945, 0, 0, 0, 0, 5154, 25), /* Sealed Vessel */
(0, 10, 1296, 0, 0, 0, 0, 5154, 25), /* Fairy Booster */
(0, 1, 284, 0, 0, 0, 0, 5154, 25), /* Amulet of Reinforcement */
(0, 1, 1904, 0, 0, 0, 0, 5154, 25); /* Tarot Card Game */


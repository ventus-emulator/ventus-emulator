INSERT INTO [main_ventus].[dbo].[RollGeneratedItem] (
	[IsRareRandom],
	[ItemGeneratedAmount],
	[ItemGeneratedVNum],
	[ItemGeneratedDesign],
	[MaximumOriginalItemRare],
	[MinimumOriginalItemRare],
	[OriginalItemDesign],
	[OriginalItemVNum],
	[Probability]
)
VALUES
/*Schau es dir von oben ab, es ist in genau der Reihenfolge*/

(0, 1, 9100, 0, 0, 0, 0, 1462, 1), /* Onyx Wings */
(0, 1, 9143, 0, 0, 0, 0, 1462, 1), /* Inventory Expansion Ticket (Perm) */
(0, 1, 9123, 0, 0, 0, 0, 1462, 1), /* Adventurer's Knapsack (Perm) */
(0, 1, 5679, 0, 0, 0, 0, 1462, 10), /* Adventurer Medal */
(0, 1, 5678, 0, 0, 0, 0, 1462, 10), /* Erenia Medal */
(0, 250, 1030, 0, 0, 0, 0, 1462, 50), /* Fullmoon crystals */
(0, 250, 2282, 0, 0, 0, 0, 1462, 50), /* Angel feathers */
(0, 20, 1286, 0, 0, 0, 0, 1462, 50), /* Ancelloan blessing */
(0, 50, 1249, 0, 0, 0, 0, 1462, 50), /* Experience Potion */
(0, 25, 1253, 0, 0, 0, 0, 1462, 50), /* Chilsug Present */
(0, 1, 9089, 0, 0, 0, 0, 1462, 4); /* Titan Wings*/

﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using System;
using System.Linq;

namespace OpenNos.GameObject
{
    public class NoFunctionItem : Item
    {
        #region Instantiation

        public NoFunctionItem(ItemDTO item) : base(item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte Option = 0, string[] packetsplit = null)
        {
            if (session.Character.IsVehicled)
            {
                session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("CANT_DO_VEHICLED"), 10));
                return;
            }

            if (session.CurrentMapInstance.MapInstanceType == Domain.MapInstanceType.TalentArenaMapInstance)
            {
                return;
            }

            switch (Effect)
            {
                case 3030:
                    switch (EffectValue)
                    {
                        case 1:
                            if (session.Character.Trophy1 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy1 += 20002;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 2:
                            if (session.Character.Trophy2 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy2 += 20003;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 3:
                            if (session.Character.Trophy3 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy3 += 20004;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 4:
                            if (session.Character.Trophy4 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy4 += 20005;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 5:
                            if (session.Character.Trophy5 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy5 += 20006;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 6:
                            if (session.Character.Trophy6 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy6 += 20007;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 7:
                            if (session.Character.Trophy7 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy7 += 20008;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 8:
                            if (session.Character.Trophy8 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy8 += 20009;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 9:
                            if (session.Character.Trophy9 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy9 += 20010;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 10:
                            if (session.Character.Trophy10 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy10 += 20011;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 11:
                            if (session.Character.Trophy11 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy11 += 20018;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 12:
                            if (session.Character.Trophy12 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy12 += 20019;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 13:
                            if (session.Character.Trophy13 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy13 += 20020;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 14:
                            if (session.Character.Trophy14 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy14 += 20021;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 15:
                            if (session.Character.Trophy15 > 0)
                            {
                                session.SendPacket("msg 4 You already have this Trophy!");
                                return;
                            }
                            session.Character.Trophy15 += 20022;
                            session.Character.TrophyCount += 1;
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;
                    }
                    break;

                case 10:
                    {
                        switch (EffectValue)
                        {
                            case 1:
                                if (session.Character.Inventory.CountItem(1036) < 1 || session.Character.Inventory.CountItem(1013) < 1)
                                {
                                    return;
                                }
                                session.Character.Inventory.RemoveItemAmount(1036);
                                session.Character.Inventory.RemoveItemAmount(1013);
                                if (ServerManager.RandomNumber() < 25)
                                {
                                    switch (ServerManager.RandomNumber(0, 2))
                                    {
                                        case 0:
                                            session.Character.GiftAdd(1015, 1);
                                            break;
                                        case 1:
                                            session.Character.GiftAdd(1016, 1);
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                if (session.Character.Inventory.CountItem(1038) < 1 || session.Character.Inventory.CountItem(1013) < 1)
                                {
                                    return;
                                }
                                session.Character.Inventory.RemoveItemAmount(1038);
                                session.Character.Inventory.RemoveItemAmount(1013);
                                if (ServerManager.RandomNumber() < 25)
                                {
                                    switch (ServerManager.RandomNumber(0, 4))
                                    {
                                        case 0:
                                            session.Character.GiftAdd(1031, 1);
                                            break;
                                        case 1:
                                            session.Character.GiftAdd(1032, 1);
                                            break;
                                        case 2:
                                            session.Character.GiftAdd(1033, 1);
                                            break;
                                        case 3:
                                            session.Character.GiftAdd(1034, 1);
                                            break;
                                    }
                                }
                                break;


                            case 3:
                                if (session.Character.Inventory.CountItem(1037) < 1 || session.Character.Inventory.CountItem(1013) < 1)
                                {
                                    return;
                                }
                                session.Character.Inventory.RemoveItemAmount(1037);
                                session.Character.Inventory.RemoveItemAmount(1013);
                                if (ServerManager.RandomNumber() < 25)
                                {
                                    switch (ServerManager.RandomNumber(0, 17))
                                    {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                        case 4:
                                            session.Character.GiftAdd(1017, 1);
                                            break;
                                        case 5:
                                        case 6:
                                        case 7:
                                        case 8:
                                            session.Character.GiftAdd(1018, 1);
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                            session.Character.GiftAdd(1019, 1);
                                            break;
                                        case 12:
                                        case 13:
                                            session.Character.GiftAdd(1020, 1);
                                            break;
                                        case 14:
                                            session.Character.GiftAdd(1021, 1);
                                            break;
                                        case 15:
                                            session.Character.GiftAdd(1022, 1);
                                            break;
                                        case 16:
                                            session.Character.GiftAdd(1023, 1);
                                            break;
                                    }
                                }
                                break;
                        }

                        session.Character.GiftAdd(1014, (byte)ServerManager.RandomNumber(5, 11));
                    }
                    break;
                default:
                    Logger.Warn(string.Format(Language.Instance.GetMessageFromKey("NO_HANDLER_ITEM"), GetType(), VNum, Effect, EffectValue));
                    break;
            }
        }

        #endregion
    }
}